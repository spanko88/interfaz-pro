import React from 'react'
import Head from 'next/head'
import { ThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

import { Provider, useSelector } from 'react-redux'
import { createWrapper } from 'next-redux-wrapper'

import store from '~/store/store'
import light from '~/theme/lightTheme'
import dark from '~/theme/darkTheme'

interface IState {
	styleMode:boolean;
}

function MyApp ( props ) {
	const { Component, pageProps } = props
	const mode = useSelector( ( state:IState ) => state.styleMode )

	const theme = mode ? dark : light

	React.useEffect( () => {
		const jssStyles = document.querySelector( '#jss-server-side' )
		if ( jssStyles ) {
			jssStyles.parentElement.removeChild( jssStyles )
		}
	}, [] )

	return (
		<React.Fragment>
			<Head>
				<title>My page</title>
				<meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
			</Head>
			<Provider store={ store }>
				<ThemeProvider theme={ theme }>
					<CssBaseline />
					<Component { ...pageProps } />
				</ThemeProvider>
			</Provider>
		</React.Fragment>
	)
}

const makestore = () => store
const wrapper = createWrapper( makestore )

export default wrapper.withRedux( MyApp )
