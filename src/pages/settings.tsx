import Head from 'next/head'
import { Dashboard1 as Dashboard } from '~/layouts'
import Settings from '~/views/settings'

const Component = () => {
	return (
		<>
			<Head>
				<title>Configuración</title>
			</Head>
			<Dashboard>
				<Settings />
			</Dashboard>
		</>
	)
}
export default Component
