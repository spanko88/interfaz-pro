import Head from 'next/head'
import { Dashboard1 as Dashboard } from '~/layouts'
import { Typography } from '@material-ui/core'

const Component = () => {
	return (
		<>
			<Head>
				<title>Usuarios</title>
			</Head>
			<Dashboard>
				<section>
					<Typography paragraph sx={ { textAlign: 'justify' } }>
                        Usuarios
					</Typography>
				</section>
			</Dashboard>
		</>
	)
}
export default Component
