import { ChangeEvent, useState, useEffect } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Paper, FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import styleMode from '~/store/actions/changeStyleMode'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	paper: {
		padding: theme.spacing( 2 ),
		marginBottom: theme.spacing( 2 )
	}
} ) )

const Component = () => {
	const classes = useStyles()
	const [mode, setMode] = useState( null )
	const dispatch = useDispatch()

	const handleChangeMode = ( e:ChangeEvent<HTMLInputElement> ) => {
		localStorage.setItem( 'mode', e.target.value )
		if ( e.target.value === 'light' ) dispatch( styleMode( false ) )
		else if ( e.target.value === 'dark' ) dispatch( styleMode( true ) )
		else {
			const event = window.matchMedia( '(prefers-color-scheme: dark)' )
			dispatch( styleMode( event.matches ) )
		}
		setMode( e.target.value )
	}

	useEffect( () => {
		const modeStorage = localStorage.getItem( 'mode' )
		setMode( modeStorage || 'light' )
	}, [] )

	return (
		<>
			<Paper className={ classes.paper }>
				{
					mode &&
					<FormControl component="fieldset">
						<FormLabel component="legend">Modo Oscuro</FormLabel>
						<RadioGroup row aria-label="modo oscuro" name="dark_mode" value={ mode } onChange={ handleChangeMode }>
							<FormControlLabel value="light" control={ <Radio /> } label="Claro" />
							<FormControlLabel value="dark" control={ <Radio /> } label="Oscuro" />
							<FormControlLabel value="auto" control={ <Radio /> } label="Automático" />
						</RadioGroup>
					</FormControl>
				}
			</Paper>

			{/* <Paper className={ classes.paper }>
				<FormControl component="fieldset">
					<FormLabel component="legend">Modo Oscuro</FormLabel>
					<RadioGroup row aria-label="modo oscuro" name="dark_mode">
						<FormControlLabel value="dark" control={ <Radio /> } label="Oscuro" />
						<FormControlLabel value="light" control={ <Radio /> } label="Claro" />
						<FormControlLabel value="auto" control={ <Radio /> } label="Automático" />
					</RadioGroup>
				</FormControl>
			</Paper> */}
		</>
	)
}

export default Component
