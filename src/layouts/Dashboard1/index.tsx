import { FC, useState, useEffect, ReactNode } from 'react'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import { Box, Drawer, AppBar, Hidden, IconButton, Avatar, Typography, Badge, Menu, MenuItem } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import { motion } from 'framer-motion'

import MenuIcon from '@material-ui/icons/Menu'
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined'
import ShopIcon from '@material-ui/icons/ShoppingCartOutlined'
import SearchIcon from '@material-ui/icons/Search'

import styleMode from '~/store/actions/changeStyleMode'
import SideNavBar from './components/SideNavBar'
import Footer from './components/Footer'

import { drawer1WidthMin, drawer1WidthMax, appBarHeight, footerHeight } from '../config'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	root: {
		display: 'flex'
	},
	content: {
		marginTop: appBarHeight,
		width: '100%',
		overflowY: 'scroll',
		height: `calc(100vh - ${appBarHeight}px)`,
		scrollbarWidth: 'none',
		msOverflowStyle: 'none',
		'&::-webkit-scrollbar': {
			display: 'none'
		}
	},
	toolbar: {
		height: appBarHeight,
		display: 'flex',
		alignItems: 'center',
		padding: ` 0 ${theme.spacing( 3 )}`
	},
	appBar: {
		marginLeft: 0,
		borderBottom: 'none',
		width: '100%',
		height: appBarHeight,
		// borderBottom: `solid 1px ${theme.palette.divider}`,
		backgroundColor: theme.palette.background.default,
		[theme.breakpoints.up( 'sm' )]: {
			marginLeft: drawer1WidthMin,
			width: `calc(100% - ${drawer1WidthMin}px)`,
			transition: theme.transitions.create( ['width', 'margin'], {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen
			} )
		}
	},
	appBarShiftLeft: {
		[theme.breakpoints.up( 'sm' )]: {
			marginLeft: drawer1WidthMax,
			width: `calc(100% - ${drawer1WidthMax}px)`,
			transition: theme.transitions.create( ['width', 'margin'], {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.enteringScreen
			} )
		}
	},
	drawerClose: {
		[theme.breakpoints.up( 'sm' )]: {
			width: drawer1WidthMin,
			transition: theme.transitions.create( 'width', {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.leavingScreen
			} )
		}
	},
	drawerOpen: {
		[theme.breakpoints.up( 'sm' )]: {
			width: drawer1WidthMax,
			transition: theme.transitions.create( 'width', {
				easing: theme.transitions.easing.sharp,
				duration: theme.transitions.duration.enteringScreen
			} )
		}
	},
	drawer: {
		backgroundColor: theme.palette.primary.main,
		border: 'none',
		whiteSpace: 'nowrap',
		zIndex: 3000,
		boxSizing: 'border-box',
		scrollbarWidth: 'none',
		msOverflowStyle: 'none',
		overflowX: 'hidden',
		'&::-webkit-scrollbar': {
			display: 'none'
		},
		'&:hover': {
			width: drawer1WidthMax
		}
	},
	drawerPaperLeft: {
		width: drawer1WidthMax,
		boxSizing: 'border-box',
		scrollbarWidth: 'none',
		msOverflowStyle: 'none',
		zIndex: 10000,
		'&::-webkit-scrollbar': {
			display: 'none'
		}
	},
	children: {
		padding: theme.spacing( 3 ),
		width: '100%',
		minHeight: `calc(100vh - ${appBarHeight}px - ${footerHeight}px)`
	},
	userTitle: {
		color: theme.palette.text.primary,
		fontSize: '12px'
	},
	userSubtitle: {
		color: theme.palette.text.primary,
		fontSize: '10px'
	},
	logout: {
		// backgroundColor: theme.palette.error.dark,
		margin: '8px',
		borderRadius: '9px'
	}
} ) )

interface IComponent {
	children:ReactNode
}

const Component:FC<IComponent> = ( { children } ) => {
	const classes = useStyles()
	const dispatch = useDispatch()
	const [openDrawerLeft, setOpenDrawerLeft] = useState( false )

	const [anchorEl, setAnchorEl] = useState<null | HTMLElement>( null )
	const open = Boolean( anchorEl )
	const handleClick = ( event:any ) => {
		setAnchorEl( event.currentTarget )
	}
	const handleClose = () => {
		setAnchorEl( null )
	}

	const handleDrawerLeftOpen = () => {
		localStorage.setItem( 'drawer', !openDrawerLeft ? 'open' : 'close' )
		setOpenDrawerLeft( state => !state )
	}

	const handleDrawerLeftClose = () => {
		setOpenDrawerLeft( false )
	}

	useEffect( () => {
		function callback ( e ) {
			const modeStorage = localStorage.getItem( 'mode' )
			if ( !modeStorage ) {
				localStorage.setItem( 'mode', 'light' )
				dispatch( styleMode( false ) )
			} else if ( modeStorage === 'auto' ) {
				dispatch( styleMode( e.matches ) )
			}
		}

		const event = window.matchMedia( '(prefers-color-scheme: dark)' )
		event.addEventListener( 'change', callback )

		return () => {
			event.removeEventListener( 'change', callback )
		}
	}, [] )

	useEffect( () => {
		const modeStorage = localStorage.getItem( 'mode' )
		if ( !modeStorage ) {
			localStorage.setItem( 'mode', 'light' )
			dispatch( styleMode( false ) )
		} else if ( modeStorage === 'light' ) {
			dispatch( styleMode( false ) )
		} else if ( modeStorage === 'dark' ) {
			dispatch( styleMode( true ) )
		} else if ( modeStorage === 'auto' ) {
			const event = window.matchMedia( '(prefers-color-scheme: dark)' )
			dispatch( styleMode( event.matches ) )
		}
	}, [] )

	useEffect( () => {
		const drawerStorage = localStorage.getItem( 'drawer' )
		if ( !drawerStorage ) {
			localStorage.setItem( 'drawer', 'close' )
			setOpenDrawerLeft( false )
		} else if ( drawerStorage === 'close' ) {
			setOpenDrawerLeft( false )
		} else if ( drawerStorage === 'open' ) {
			setOpenDrawerLeft( true )
		}
	}, [] )

	return (
		<div className={ classes.root }>
			<AppBar
				position="fixed"
				elevation={ 0 }
				className={ clsx( classes.appBar, { [classes.appBarShiftLeft]: openDrawerLeft } ) }>
				<Box className={ classes.toolbar }>
					<IconButton edge="end" onClick={ handleDrawerLeftOpen } size='small'><MenuIcon /></IconButton>
					<Box sx={ { flexGrow: 1 } } />
					<IconButton size='small' sx={ { marginRight: '16px' } }>
						<SearchIcon />
					</IconButton>
					<IconButton size='small' sx={ { marginRight: '16px' } }>
						<Badge badgeContent={ 1 } color='primary' overlap="circular">
							<ShopIcon />
						</Badge>
					</IconButton>
					<IconButton size='small' sx={ { marginRight: '16px' } }>
						<Badge badgeContent={ 4 } color='error' overlap="circular">
							<NotificationsIcon />
						</Badge>
					</IconButton>
					<Box
						sx={ { display: 'flex', justifyContent: 'center', alignItems: 'center', cursor: 'pointer' } }
						onClick={ handleClick }
					>
						<Hidden smDown>
							<Box sx={ { display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', marginRight: '10px' } }>
								<Typography variant='subtitle2' className={ classes.userTitle }>Angelina Jolie</Typography>
								<Typography variant='subtitle2' className={ classes.userSubtitle }>Administrador</Typography>
							</Box>
						</Hidden>
						<Avatar src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS5eIWdZAkzM0I9-vpnDMECefs8fltxgPgGsg&usqp=CAU' />
					</Box>
					<Menu
						id="basic-menu"
						anchorEl={ anchorEl }
						open={ open }
						onClose={ handleClose }
						MenuListProps={
							{
								'aria-labelledby': 'basic-button'
							}
						}
					>
						<MenuItem onClick={ handleClose } className={ classes.logout }>Cuenta</MenuItem>
						<MenuItem onClick={ handleClose } className={ classes.logout }>Salir</MenuItem>
					</Menu>
				</Box>
			</AppBar>

			<Hidden smDown>
				<Drawer
					variant="permanent"
					className={ clsx( classes.drawer, { [classes.drawerOpen]: openDrawerLeft, [classes.drawerClose]: !openDrawerLeft } ) }
					classes={ { paper: clsx( classes.drawer, { [classes.drawerOpen]: openDrawerLeft, [classes.drawerClose]: !openDrawerLeft } ) } }
				>
					<SideNavBar />
				</Drawer>
			</Hidden>
			<Hidden smUp>
				<Drawer
					variant="temporary"
					open={ openDrawerLeft }
					onClose={ handleDrawerLeftClose }
					classes={ { paper: clsx( classes.drawer, classes.drawerPaperLeft ) } }
					anchor="left"
				>
					<SideNavBar />
				</Drawer>
			</Hidden>
			<motion.main
				className={ classes.content }
				initial={ { opacity: 0 } }
				animate={ { opacity: 1 } }
				exit={ { opacity: 0 } }
				transition={ { duration: 1.6, ease: 'easeIn' } }
				// transition={ { duration: 1.2, ease: [0.53, 1, 0.6, 1.26] } }
			>
				<section className={ classes.children }>
					{children}
				</section>
				<Footer />
			</motion.main>
		</div>
	)
}

export default Component
