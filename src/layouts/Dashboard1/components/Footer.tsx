import { Typography } from '@material-ui/core'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { footerHeight } from '../../config'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	container: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		padding: '6px 16px 0',
		width: '100%',
		height: footerHeight + 'px',
		backgroundColor: theme.palette.common.black
	},
	a: {
		textDecoration: 'none',
		color: '#fff',
		display: 'flex',
		justifyContent: 'space-evenly',
		width: '100%'
	},
	text: {
		fontSize: '12px'
	}
} ) )

const Component = () => {
	const classes = useStyles()
	return (
		<footer className={ classes.container }>
			<a
				className={ classes.a }
				href='https://www.erikrodriguezguerrero.com'
				rel="noopener noreferrer"
				target="_blank"
			>
				<Typography className={ classes.text }>
					Develop by Erik Rodríguez
				</Typography>
				<Typography className={ classes.text }>
					2021
				</Typography>
			</a>
		</footer>
	)
}

export default Component
