import Link from 'next/link'
import { useRouter } from 'next/router'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Box, List, Typography, Icon, Divider } from '@material-ui/core'
import clsx from 'clsx'
import useDimensions from 'react-cool-dimensions'
import { motion, AnimatePresence, AnimationProps } from 'framer-motion'

import { appBarHeight } from '../../config'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	logo: {
		height: appBarHeight,
		backgroundColor: theme.palette.primary.main,
		display: 'flex',
		justifyContent: 'flex-start',
		alignItems: 'center',
		padding: '0 11px',
		color: '#fff'
		// borderBottom: `solid 1px ${theme.palette.divider}`
	},
	list: {
		padding: '10px',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'space-between',
		height: `calc(100% - ${appBarHeight}px)`
	},
	listItem: {
		borderRadius: '14px',
		margin: '4px 0',
		display: 'flex',
		alignItems: 'center',
		padding: '8px 12px'
	},
	listItemActive: {
		color: theme.palette.primary.main,
		backgroundColor: '#fff'
	},
	listItemInactive: {
		color: '#fff',
		'&:hover': {
			backgroundColor: theme.palette.primary.light
		}
	}
} ) )

const variants:AnimationProps['variants'] = {
	initial: { x: '-200px', opacity: 0 },
	animate: { x: 0, opacity: 1, transition: { x: { duration: 0.5, ease: 'easeInOut' }, opacity: { duration: 0.7, ease: 'easeIn' } } },
	exit: { x: '-200px', opacity: 0, transition: { x: { duration: 0.5, ease: 'easeInOut' }, opacity: { duration: 0.3, ease: 'easeIn' } } }
}

const ListItemArray = [
	{ label: 'Inicio', href: '/home', icon: 'home' },
	{ label: 'Usuarios', href: '/users', icon: 'person' }
]

const Component = () => {
	const classes = useStyles()
	const route = useRouter()

	const { observe, width: wd } = useDimensions( {
		onResize: ( { observe, unobserve, width, height, entry } ) => {
		// Triggered whenever the size of the target is changed...

			unobserve() // To stop observing the current target element
			observe() // To re-start observing the current target element
		}
	} )

	return (
		<Box ref={ observe } sx={ { height: '100%' } }>
			<Box className={ classes.logo }>
				<Link href={ '/' }>
					<a style={ { textDecoration: 'none', width: '100%' } }>
						<Box className={ clsx( classes.logo ) }>
							<Icon>light</Icon>
							<AnimatePresence exitBeforeEnter>
								{
									wd > 200 && (
										<motion.span
											variants={ variants }
											initial={ 'initial' }
											animate={ 'animate' }
											exit={ 'exit' }
										>
											<Typography sx={ { marginLeft: '12px' } }>Logo</Typography>
										</motion.span>
									)
								}
							</AnimatePresence>
						</Box>
					</a>
				</Link>
			</Box>
			<List className={ classes.list }>
				{
					ListItemArray.map( ( item, index ) => (
						<Link href={ item.href } key={ item.label }>
							<a style={ { textDecoration: 'none' } }>
								<Box className={ clsx( classes.listItem, { [classes.listItemActive]: route.route === item.href, [classes.listItemInactive]: route.route !== item.href } ) }>
									<Icon>{item.icon}</Icon>
									<AnimatePresence exitBeforeEnter>
										{
											wd > 200 && (
												<motion.span
													variants={ variants }
													initial={ 'initial' }
													animate={ 'animate' }
													exit={ 'exit' }
												>
													<Typography sx={ { marginLeft: '12px' } }>{item.label}</Typography>
												</motion.span>
											)
										}
									</AnimatePresence>
								</Box>
							</a>
						</Link>
					) )
				}
				<Box sx={ { flexGrow: 1 } } />
				<Divider color='#fff' />
				{
					<Link href={ '/settings' }>
						<a style={ { textDecoration: 'none' } }>
							<Box className={ clsx( classes.listItem, { [classes.listItemActive]: route.route === '/settings', [classes.listItemInactive]: route.route !== '/settings' } ) }>
								<Icon>settings</Icon>
								<AnimatePresence exitBeforeEnter>
									{
										wd > 200 && (
											<motion.span
												variants={ variants }
												initial={ 'initial' }
												animate={ 'animate' }
												exit={ 'exit' }
											>
												<Typography sx={ { marginLeft: '12px' } }>Configuración</Typography>
											</motion.span>
										)
									}
								</AnimatePresence>
							</Box>
						</a>
					</Link>
				}
			</List>
		</Box>
	)
}

export default Component
