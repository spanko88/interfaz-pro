import { FC, ReactNode } from 'react'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import { Box } from '@material-ui/core'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	tooltip: {
		display: 'inline-block',
		position: 'relative',
		'&::before,&::after': {
			position: 'absolute',
			left: '50%',
			transition: '250ms transform',
			fontSize: '14px',
			width: 'max-content'
		},
		'&::before': {
			content: ( props:any ) => `"${props.title}"`,
			color: theme.palette.background.paper,
			padding: '1px 14px',
			borderRadius: '7px',
			textAlign: 'center',
			background: theme.palette.mode === 'dark' ? '#FFF' : '#737373',
			transformOrigin: 'center top',
			transform: 'translate(-50%,-50%) scale(0)',
			top: 'calc(50% + 34px)'
		},
		'&::after': {
			content: '""',
			border: '8px solid transparent',
			borderBottomColor: theme.palette.mode === 'dark' ? '#FFF' : '#737373',
			transformOrigin: 'center bottom',
			transform: 'translate(-50%,-50%) scale(0)',
			top: 'calc(50% + 15px)'
		},
		'&:hover::before,&:hover::after': {
			transform: 'translate(-50%,-50%) scale(1)'
		}
	}
} ) )

interface IComponent {
	children?:ReactNode,
	title:string
}

const Component:FC<IComponent> = ( { children, title } ) => {
	const classes = useStyles( { title } )
	return (
		<Box className={ classes.tooltip }>
			{children}
		</Box>
	)
}

export default Component
