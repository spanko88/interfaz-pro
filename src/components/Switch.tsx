import { FC, ReactNode, useState } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { motion, Variants } from 'framer-motion'

const useStyles = makeStyles( ( theme:Theme ) => createStyles( {
	box: {
		borderRadius: '100px',
		width: '80px',
		padding: '6px'
	},
	circle: {
		borderRadius: '20px',
		width: '2rem',
		height: '2rem',
		position: 'relative',
		backgroundColor: '#fff'

	}
} ) )

const boxVariants:Variants = {
	on: {
		backgroundColor: '#DB5149'
	},
	off: {
		backgroundColor: '#e0e0e0'
	}
}

const circleVariants:Variants = {
	on: {
		left: '100%',
		x: '-100%'

	},
	off: {
		left: 0,
		x: 0
	}
}

interface IComponent {
	children?:ReactNode
}

const Component:FC<IComponent> = ( { children } ) => {
	const classes = useStyles()
	const [isOn, setIsOn] = useState( false )

	const handleIsOn = () => {
		setIsOn( ( state ) => ( !state ) )
	}

	return (
		<motion.div
			className={ classes.box }
			onClick={ handleIsOn }
			variants={ boxVariants }
			animate={ isOn ? 'on' : 'off' }
			transition={ { duration: 0.5, ease: 'easeInOut' } }

		>
			<motion.div
				className={ classes.circle }
				variants={ circleVariants }
			/>
		</motion.div>
	)
}

export default Component
