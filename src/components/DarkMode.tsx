import { FC } from 'react'
import { IconButton } from '@material-ui/core'
import { motion, Variants } from 'framer-motion'
import Tooltip from '~/components/Tooltip'

const variants:Variants = {
	hidden: {
		opacity: 0,
		scale: 0,
		display: 'none'
	},
	show: {
		opacity: 1,
		scale: 1,
		display: 'block',
		rotate: 360
	}
}
interface IComponent {
	onChange?:( )=>void,
	value?:boolean,
}

const Component:FC<IComponent> = ( { onChange, value } ) => {
	const modeColor = 'currentColor'
	return (
		<Tooltip title={ !value ? 'Noche' : 'Día' }>
			<IconButton
				size='small'
				onClick={ onChange }
			>
				<motion.svg
					width="30"
					height="30"
					viewBox="0 0 30 30"
					fill="none"
					xmlns="http://www.w3.org/2000/svg"
					variants={ variants }
					animate={ value ? 'show' : 'hidden' }
					transition={ { duration: 1, ease: 'easeInOut' } }
				>
					<g id="luna">
						<path id="Ellipse 1" fillRule="evenodd" clipRule="evenodd" d="M15 24C19.9706 24 24 19.9706 24 15C24 11.8695 22.4017 9.11238 19.9767 7.5C19.9767 7.5 20.9767 13.5 17 17C13.0233 20.5 8.5 21.225 8.5 21.225C10.1382 22.9351 12.4448 24 15 24Z" fill={ modeColor } />
					</g>
				</motion.svg>
				<motion.svg
					width="30"
					height="30"
					viewBox="0 0 30 30"
					fill="none"
					xmlns="http://www.w3.org/2000/svg"
					variants={ variants }
					animate={ value ? 'hidden' : 'show' }
					transition={ { duration: 1, ease: 'easeInOut' } }
				>
					<g id="sol">
						<path id="Ellipse 1" d="M24 15C24 19.9706 19.9706 24 15 24C12.4448 24 10.1382 22.9351 8.5 21.225C6.95145 19.6084 6 17.4153 6 15C6 10.0294 10.0294 6 15 6C16.8401 6 18.5512 6.55223 19.9767 7.5C22.4017 9.11238 24 11.8695 24 15Z" fill={ modeColor } fillOpacity="0.98" />
						<path id="Rectangle 12" fillRule="evenodd" clipRule="evenodd" d="M10 6H6V10L10 6Z" fill={ modeColor } fillOpacity="0.98" />
						<path id="Rectangle 13" fillRule="evenodd" clipRule="evenodd" d="M20 24H24V20L20 24Z" fill={ modeColor } fillOpacity="0.98" />
						<path id="Rectangle 15" fillRule="evenodd" clipRule="evenodd" d="M6 20V24H10L6 20Z" fill={ modeColor } fillOpacity="0.98" />
						<path id="Rectangle 16" fillRule="evenodd" clipRule="evenodd" d="M4.82843 12L2 14.8284L4.82843 17.6569V12Z" fill={ modeColor } fillOpacity="0.98" />
						<path id="Rectangle 17" fillRule="evenodd" clipRule="evenodd" d="M24.8284 17.6569L27.6569 14.8284L24.8284 12V17.6569Z" fill={ modeColor } fillOpacity="0.98" />
						<path id="Rectangle 18" fillRule="evenodd" clipRule="evenodd" d="M17.6568 4.82843L14.8284 2L11.9999 4.82843H17.6568Z" fill={ modeColor } fillOpacity="0.98" />
						<path id="Rectangle 19" fillRule="evenodd" clipRule="evenodd" d="M11.9999 24.8284L14.8284 27.6569L17.6568 24.8284H11.9999Z" fill={ modeColor } fillOpacity="0.98" />
						<path id="Rectangle 14" fillRule="evenodd" clipRule="evenodd" d="M24 10V6H20L24 10Z" fill={ modeColor } fillOpacity="0.98" />
					</g>
				</motion.svg>
			</IconButton>
		</Tooltip>
	)
}

export default Component
