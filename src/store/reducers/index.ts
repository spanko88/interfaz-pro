import { combineReducers } from 'redux'
import styleMode from './styleMode'

export default combineReducers( {
	styleMode: styleMode
} )
