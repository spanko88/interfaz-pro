import * as t from '~/store/types'
const initialState:boolean = false

const styleMode = ( state = initialState, { type } ) => {
	switch ( type ) {
		case t.LIGHTMODE :
			return false

		case t.DARKMODE :
			return true

		default :
			return state
	}
}

export default styleMode
