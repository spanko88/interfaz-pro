import * as t from '~/store/types'

const changeStyleMode = ( value:boolean ) => dispatch => {
	if ( value === false ) {
		dispatch( {
			type: t.LIGHTMODE
		} )
	} else {
		dispatch( {
			type: t.DARKMODE
		} )
	}
}

export default changeStyleMode
