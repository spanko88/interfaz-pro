import { createMuiTheme } from '@material-ui/core/styles'
import palette from './palette'
import components from './muicomponents'

const theme = createMuiTheme( {
	palette: {
		mode: 'light',
		...palette
	},
	components
} )

export default theme
