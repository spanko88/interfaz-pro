import { ThemeOptions } from '@material-ui/core/styles'

const components:ThemeOptions['components'] = {
	MuiTypography: {
		styleOverrides: {
			root: {
				fontFamily: 'Lexend'
			}
		}
	},
	MuiCssBaseline: {
		styleOverrides: {
			body: {
				width: '100vw',
				height: '100vh',
				overflow: 'hidden'
			}
		}
	}
}

export default components
