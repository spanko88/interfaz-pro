import { PaletteOptions } from '@material-ui/core/styles'
import { red } from '@material-ui/core/colors'

const palette:PaletteOptions = {
	primary: {
		main: '#6F6AC0'
	},
	secondary: {
		main: '#19857b'
	},
	error: {
		main: red.A400
	}
}

export default palette
